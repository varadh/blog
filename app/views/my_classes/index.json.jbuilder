json.array!(@my_classes) do |my_class|
  json.extract! my_class, :id
  json.url my_class_url(my_class, format: :json)
end
