require 'test_helper'

class MyClassesControllerTest < ActionController::TestCase
  setup do
    @my_class = my_classes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:my_classes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create my_class" do
    assert_difference('MyClass.count') do
      post :create, my_class: {  }
    end

    assert_redirected_to my_class_path(assigns(:my_class))
  end

  test "should show my_class" do
    get :show, id: @my_class
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @my_class
    assert_response :success
  end

  test "should update my_class" do
    patch :update, id: @my_class, my_class: {  }
    assert_redirected_to my_class_path(assigns(:my_class))
  end

  test "should destroy my_class" do
    assert_difference('MyClass.count', -1) do
      delete :destroy, id: @my_class
    end

    assert_redirected_to my_classes_path
  end
end
